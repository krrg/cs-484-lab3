#!/usr/bin/env python

import random
from mpi4py import MPI
comm = MPI.COMM_WORLD


def broadcast(dimensions, data=None):
    bitmask = 2**(dimensions - 1)
    notParticipating = bitmask - 1

    for curdim in xrange(dimensions):
        if comm.rank & notParticipating == 0:
            if comm.rank & bitmask == 0:
                dest = comm.rank ^ bitmask
                comm.send(data, dest=dest)
            else:
                src = comm.rank ^ bitmask
                data = comm.recv(source=src)
        notParticipating >>= 1
        bitmask >>= 1

    return data


def reduction(dimensions, data, fn):
    notParticipating = 0
    bitmask = 1

    for curdim in xrange(dimensions):
        if comm.rank & notParticipating == 0:
            if comm.rank & bitmask != 0:
                dest = comm.rank ^ bitmask
                comm.send(data, dest=dest)
            else:
                src = comm.rank ^ bitmask
                newdata = comm.recv(source=src)
                print "Node {} received {}".format(comm.rank, newdata)
                data = [fn(*e) for e in zip(data, newdata)]
                print "Node {} computed {}".format(comm.rank, data)
        notParticipating = notParticipating ^ bitmask
        bitmask <<= 1

    return data


def get_rand_array(size):
    return map(lambda i: random.randint(1, 2**7), xrange(size))


reduction(4, get_rand_array(8), lambda x, y: max(x, y))

comm.Barrier()
